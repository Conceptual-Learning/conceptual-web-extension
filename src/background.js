import low from "lowdb/lib/fp";
import { map, empty, append, head, mergeRight } from "ramda";
import ua_parser from "ua-parser-js";
import { LocalStorage } from "./WebExtension";
import { init as api_init, log_activity } from "../target/build/api_adapter.core"
import { merge } from "json-merge-patch";


// CURRENT PAGE RECORD AND METHODS
const currentPage = { url: null, start_time: null, duration: null };

// CRITICAL get the current tab's url
const getCurrentTabUrl = () =>
  browser.tabs
    .query({ currentWindow: true, active: true })
    .then(tabs => head(tabs).url);

const setCurrentUrl = (url) => {
  currentPage["url"] = url;
  currentPage["start_time"] = new Date();
  currentPage["duration"] = null;
};

const createRecord = () => {
  return {
    payload: { url: currentPage["url"] },
    start_time: currentPage["start_time"].toUTCString(),
    duration: ((new Date() - currentPage["start_time"]) / 1000).toString(),
  };
};

const clearCurrentPage = () => {
  currentPage["url"] = null;
  currentPage["start_time"] = null;
  currentPage["duration"] = null;
};

// PERSISTENT DB METHODS
low(new LocalStorage("conceptual")).then(db => {
  // init low db
  const records = db("records", []);
  api_init();

  const parsed_agent = ua_parser(navigator.userAgent);
  console.log(navigator.userAgent, parsed_agent);

  const details = {
    activity: "web browsing",
    device: mergeRight(parsed_agent.device, { os: parsed_agent.os })
  };

  console.log(details);

  const syncRecords = () => {
    const records_to_sync = records(map(mergeRight(details)));
    console.debug("SYNC TO DB:", records_to_sync);

    // Write records to the DB
    log_activity(records_to_sync, ({ data, errors }) => {
      if (data) {
        console.debug("SYNC SUCCESS:", data);
        records.write(empty);
      } else if (errors) {
        console.debug("SYNC FAIL:", errors)
      }
    })
  };

  const http_url_re = new RegExp('^(http://|https://)[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$')
  const writeRecord = () => {
    if (currentPage.start_time && http_url_re.test(currentPage.url))
      records.write(append(createRecord()));
    // logRecords();
  };

  const logRecords = () => console.log(records([]));

  // EVENT LISTENERS


  // DESKTOP + MOBILE

  // capture url changes
  browser.tabs.onUpdated.addListener((tabId, changeInfo, tabInfo) => {
    if (changeInfo.url) {
      console.debug("EVENT: URL CHANGED ", changeInfo.url);
      writeRecord();
      setCurrentUrl(changeInfo.url);
    }
  });

  // capture tab switches
  browser.tabs.onActivated.addListener(e => {
    console.debug("EVENT: SWITCHED TAB ", e);
    writeRecord();
    getCurrentTabUrl().then(setCurrentUrl);
  });

  // DESKTOP

  // if windows out of focus (e == -1) then push record and reset current page
  // else set current page
  browser.windows.onFocusChanged.addListener(e => {
    switch (e) {
      case -1:
        console.debug("EVENT: WINDOW OUT OF FOCUS");
        writeRecord();
        clearCurrentPage();
        // sync is a bit heavy so we are limiting its use to only when the browser exit
        syncRecords();
        break;
      default:
        console.debug("EVENT: WINDOW IN FOCUS");
        getCurrentTabUrl().then(setCurrentUrl);
    }
  });


});
